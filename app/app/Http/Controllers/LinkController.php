<?php

namespace App\Http\Controllers;

use App\Services\LinkService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class LinkController extends Controller
{
    private $service;

    public function __construct(LinkService $service)
    {
        $this->service = $service;
    }

    public function index(): View
    {
        return view('link.index');
    }

    public function token($token)
    {
        if ($link = $this->service->getLink($token)) {
            return redirect($link);
        }

        return abort(404);
    }

    public function generateLink(Request $request): JsonResponse
    {
        $link = $this->service->createLink($request->all());

        return response()->json([
            view('link._generated-link', [
                'link' => $link,
            ])->render()
        ]);
    }
}