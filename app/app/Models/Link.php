<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Link
 * @property int $id
 * @property string $source
 * @property string $token
 * @property Carbon|null $time_of_death
 * @property int $max_hits
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 */
class Link extends Model
{
    protected $table = 'links';
}