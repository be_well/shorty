<?php

namespace App\Services;

use App\Models\Link;

class LinkService
{
    public function createLink(array $fields): string
    {
        $link = new Link();
        $link->source = $fields['link'];
        $link->token = $this->generateToken(8);
        $link->time_of_death = $this->getExpirationTime((int) $fields['expire']);
        $link->max_hits = ($fields['hits']) ? (int) $fields['hits'] : null;
        $link->save();

        return \config('env_values.app_url') . '/' . $link->token;
    }

    private function generateToken(int $length): string
    {
        $charsUpper = range('A','Z');
        $charsLower = range('a','z');
        $numbers = range('0','9');
        $data = array_merge($charsUpper, $charsLower, $numbers);
        $token = '';

        while ($length--) {
            $token .= $data[array_rand($data)];
        }

        return $token;
    }

    private function getExpirationTime(int $expire): ?\DateTime
    {
        if ($expire !== 0) {
            $date = new \DateTime();
            $date->add(new \DateInterval('PT' . $expire . 'H'));

            return $date;
        }

        return null;
    }

    public function getLink(string $token): ?string
    {
        if (!$link = Link::where('token', $token)->first()) {
            return null;
        }

        if ($link->max_hits && ($link->max_hits <= $link->hits)) {
            return null;
        }

        $currentDate = new \DateTime();
        if ($link->time_of_death && ($link->time_of_death < $currentDate->format('Y-m-d H:i:s'))) {
            return null;
        }

        $link->hits += 1;
        $link->save();

        return $link->source;
    }
}