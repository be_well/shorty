@extends('layout')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="logo">
                <img src="/assets/shorty.svg" alt="Shorty Logo">
            </div>
            <h5 class="card-title">Make it <strong>Shorty</strong></h5>

            <form id="form" method="POST" enctype="multipart/form-data" action="{{ route('generate-link') }}" class="form">
                @csrf
                <div class="form-group">
                    <label for="source">Link</label>
                    <input type="text" class="form-control" id="source" placeholder="Paste your link here...">
                </div>

                <div class="form-group">
                    <label for="expire">Expiration Time</label>
                    <input type="number" class="form-control" id="expire" placeholder="Up to 24 hrs" min="0" max="24">
                </div>
                <div class="form-group">
                    <label for="hits">Maximum usage</label>
                    <input type="number" class="form-control" id="hits" placeholder="Set max redirect times" min="0">
                </div>
                <div class="form-group submit-button">
                    <button id="submit-form" type="submit" class="btn btn-primary">Generate link</button>
                </div>
            </form>

            <div id="generated-link" class="generated-link"></div>
        </div>
    </div>

@endsection