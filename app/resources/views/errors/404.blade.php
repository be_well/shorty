@extends('layout')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="logo">
                <img src="/assets/shorty.svg" alt="Shorty Logo">
            </div>
            <h5 class="card-title">Make it <strong>Shorty</strong></h5>

            <div class="error">
                <div class="status">404</div>
                <div class="message">Sorry. Nothing has been found</div>
            </div>
        </div>
    </div>

@endsection