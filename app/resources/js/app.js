const form = document.getElementById('form');
const source = document.getElementById('source');
const expire = document.getElementById('expire');
const hits = document.getElementById('hits');

const fetchData = async () => {
    const token = document.getElementsByName('_token');
    const data = {
        _token: token[0].value,
        link: source.value,
        expire: expire.value,
        hits: hits.value
    }
    try {
        const response = await fetch(form.getAttribute('action'), {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        });

        if (response.ok){
            return await response.json();
        }
    } catch (error) {
        console.log(error);
    }
}

const submit = async (event) => {
    event.preventDefault();

    if (source.value === '') {
        alert('Please, provide URL');
        return false;
    }

    const result = await fetchData();
    const place = document.getElementById('generated-link');
    place.innerHTML = result;
    source.value = '';
    expire.value = '';
    hits.value = '';
}

form.addEventListener('submit', submit);
