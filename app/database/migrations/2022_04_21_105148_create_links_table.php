<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->id();
            $table->text('source')->comment('Destination link');
            $table->string('token', 8)->comment('Random token');
            $table->timestamp('time_of_death')->nullable()->comment('Token life span');
            $table->unsignedInteger('max_hits')->nullable()->comment('Max redirects per link');
            $table->unsignedInteger('hits')->nullable()->comment('Total redirects');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
