<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [App\Http\Controllers\LinkController::class, 'index'])->name('index');
Route::get('/{token}', [App\Http\Controllers\LinkController::class, 'token'])->name('token');
Route::post('/generate-link', [App\Http\Controllers\LinkController::class, 'generateLink'])->name('generate-link');
