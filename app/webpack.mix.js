const mix = require('laravel-mix');

mix.sass('resources/scss/styles.scss', 'public/css').sourceMaps(true, 'source-map').version();
mix.js('resources/js/app.js', 'public/js').version();