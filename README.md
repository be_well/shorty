<img src="https://i.imgur.com/HPOOBrF.png">

# Shorty


## About Application

Shorty is a simple application based on Laravel that helps you to convert your long and ugly links into a readable compact format.

## Notes
.env already included with pre-defined values (MySQL credentials)


## Installation


#### 1. Clone this repo

```
git clone git@bitbucket.org:be_well/shorty.git
```

#### 2. Run docker-compose

```
docker-compose build && docker-compose up -d
```

#### 3. Install dependencies

```
docker-compose exec php composer install
```

#### 4. Migrate

```
docker-compose exec php php /var/www/html/artisan migrate --path 'database/migrations'
```

#### 4. Launch

```
go to localhost:8000
```

## License

This app is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
